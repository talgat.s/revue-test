package test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/talgat.s/revue-test/cmd/cli"
)

func beforeAll() func() {
	return func() {
	}
}

func beforeEach(t *testing.T) func(t *testing.T) {
	if _, err := cli.Migrate(); err != nil {
		t.Fatalf("fail migrations %v %T", err, err)
	}

	return func(t *testing.T) {
		if _, err := cli.Rollback(); err != nil {
			t.Fatalf("fail migrations %v %T", err, err)
		}
	}
}

func TestMain(m *testing.M) {
	afterAll := beforeAll()
	exitVal := m.Run()
	afterAll()

	os.Exit(exitVal)
}

func TestProductCreate(t *testing.T) {
	defer beforeEach(t)(t)
	assert.Equal(t, 123, 123, "they should be equal")
}
