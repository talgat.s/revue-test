package cli

import (
	"os"
	"os/exec"
	"path/filepath"
)

func Migrate() (string, error) {
	return run("migrate")
}

func Rollback() (string, error) {
	return run("rollback")
}

func run(arg string) (string, error) {
	dir, err := os.Getwd()
	if err != nil {
		return "", err
	}
	rootDir := filepath.Dir(dir)

	prg := "make"
	cmd := exec.Command(prg, arg)
	cmd.Dir = rootDir
	if stdout, err := cmd.Output(); err != nil {
		return "", err
	} else {
		return string(stdout), err
	}
}
