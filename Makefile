.PHONY: migrate
migrate: ../../Makefile
	$(MAKE) -C ../.. --no-print-directory test/migrate

.PHONY: rollback
rollback: ../../Makefile
	$(MAKE) -C ../.. --no-print-directory test/rollback
